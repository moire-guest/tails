# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2018-09-18 06:11+0200\n"
"PO-Revision-Date: 2018-05-10 07:25+0000\n"
"Last-Translator: Tails translators <amnesia@boum.org>\n"
"Language-Team: \n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.10.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Fri, 30 Mar 2018 17:00:00 +0000\"]]\n"
msgstr "[[!meta date=\"Fri, 30 Mar 2018 17:00:00 +0000\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails 3.6.2 is out\"]]\n"
msgstr "[[!meta title=\"Lançado o Tails 3.6.2\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
msgid ""
"This release fixes [[several security issues|security/"
"Numerous_security_holes_in_3.6.1]] and users should upgrade as soon as "
"possible."
msgstr ""
"Esta versão corrige [[diversos problemas de segurança|security/"
"Numerous_security_holes_in_3.6.1]] e os usuários devem migrar para ela o "
"quanto antes."

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=1]]\n"
msgstr "[[!toc levels=1]]\n"

#. type: Title #
#, no-wrap
msgid "Changes"
msgstr "Mudanças"

#. type: Title ##
#, no-wrap
msgid "Upgrades and changes"
msgstr "Atualizações e mudanças"

#. type: Plain text
msgid "- Update *Tor Browser* to 7.5.3."
msgstr "- *Tor Browser* foi atualizado para a versão 7.5.3."

#. type: Plain text
msgid "- Update *Thunderbird* to 52.7.0."
msgstr "- *Thunderbird* foi atualizado para 52.7.0."

#. type: Bullet: '- '
msgid ""
"Update *Intel microcode* to 3.20180312.1, which adds Spectre variant 2 "
"mitigation for most common Intel processors."
msgstr ""
"*Intel microcode* foi atualizado para 3.20180312.1, que adiciona mitigação "
"para o Spectre variante 2 para os processadores Intel mais comuns."

#. type: Plain text
msgid "- Upgrade *Linux* to 4.15.11-1."
msgstr "- *Linux* foi atualizado para 4.15.11-1."

#. type: Title ##
#, no-wrap
msgid "Fixed problems"
msgstr "Problemas resolvidos"

#. type: Bullet: '- '
msgid "Make input of Japanese and Korean characters in Tor Browser work again."
msgstr ""
"Os caracteres de entrada em japonês e coreano voltaram a funcionar no "
"Navegador Tor."

#. type: Bullet: '- '
msgid ""
"Fix Tor Browser so add-ons installed by users work again, such as Tails "
"Verification."
msgstr ""
"O Navegador Tor foi corrigido para que extensões instaladas por usuários, "
"tais como Tails Verification, funcionem novamente."

#. type: Plain text
msgid ""
"For more details, read our [[!tails_gitweb debian/changelog desc=\"changelog"
"\"]]."
msgstr ""
"Para mais detalhes, leia nosso  [[!tails_gitweb debian/changelog desc="
"\"changelog\"]]."

#. type: Plain text
#, no-wrap
msgid "<a id=\"known-issues\"></a>\n"
msgstr "<a id=\"known-issues\"></a>\n"

#. type: Plain text
msgid "See the list of [[long-standing issues|support/known_issues]]."
msgstr "Veja a lista de [[problemas de longa data|support/known_issues]]."

#. type: Title #
#, no-wrap
msgid "Get Tails 3.6.2"
msgstr "Baixe o Tails 3.6.2"

#. type: Plain text
msgid "- To install, follow our [[installation instructions|install]]."
msgstr "- Para instalar, siga nossas [[instruções de instalação|install]]."

#. type: Bullet: '- '
msgid ""
"To upgrade, automatic upgrades are available from 3.6 and 3.6.1 to 3.6.2."
msgstr ""
"Atualizações automáticas do Tails 3.6 e 3.6.1 para o 3.6.2 estão disponíveis."

#. type: Plain text
#, no-wrap
msgid ""
"  If you cannot do an automatic upgrade or if you fail to start after an\n"
"  automatic upgrade, please try to do a [[manual upgrade|upgrade]].\n"
msgstr "  Se você não conseguir fazer uma atualização automática ou se seu Tails não iniciar após uma atualização automática, tente uma [[instalação manual|upgrade]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"  If you are doing a manual upgrade from Tails 3.2, 3.3, 3.4, or 3.5, it is only possible to select an\n"
"  ISO image when running Tails in English. For other languages, the\n"
"  file chooser button remains inactive:\n"
msgstr ""
"  Se você estiver fazendo uma atualização manual a partir do Tails 3.2, 3.3, 3.4 ou 3.5, então somente será\n"
"  possível selecionar uma imagem ISO se você estiver rodando o Tails em inglês. Nos outros idiomas, o\n"
"  botão de escolha de arquivo ficará inativo:\n"

#. type: Plain text
#, no-wrap
msgid "  [[!img news/version_3.5/broken-file-chooser.png link=\"no\"]]\n"
msgstr "  [[!img news/version_3.5/broken-file-chooser.png link=\"no\"]]\n"

#. type: Plain text
#, no-wrap
msgid "  To do a manual upgrade from Tails 3.2, 3.3, 3.4, or 3.5, you can either:\n"
msgstr "  Para fazer uma atualização manual do Tails 3.2, 3.3, 3.4 ou 3.5, você pode:\n"

#. type: Bullet: '  - '
msgid "Restart Tails in English."
msgstr "Reinicie o Tails em inglês."

#. type: Bullet: '  - '
msgid ""
"Start <span class=\"application\">Tails Installer</span> in English from the "
"command line:"
msgstr ""
"Inicie o <span class=\"application\">Instalador Tails</span> em inglês a "
"partir da linha de comando:"

#. type: Plain text
#, no-wrap
msgid "    <pre>LANG=en_US tails-installer</pre>\n"
msgstr "    <pre>LANG=en_US tails-installer</pre>\n"

#. type: Plain text
msgid "- [[Download Tails 3.6.2.|install/download]]"
msgstr "- [[Baixe o Tails 3.6.2.|install/download]]"

#. type: Title #
#, no-wrap
msgid "What's coming up?"
msgstr "O que vem por aí?"

#. type: Plain text
msgid "Tails 3.7 is [[scheduled|contribute/calendar]] for May 8."
msgstr "Tails 3.7 está [[agendado|contribute/calendar]] para o dia 8 de maio."

#. type: Plain text
msgid "Have a look at our [[!tails_roadmap]] to see where we are heading to."
msgstr "Confira o nosso [[!tails_roadmap]] e veja nossos objetivos futuros."

#. type: Plain text
#, no-wrap
msgid ""
"We need your help and there are many ways to [[contribute to\n"
"Tails|contribute]] (<a href=\"https://tails.boum.org/donate/?r=3.6.2\">donating</a> is only one of\n"
"them). Come [[talk to us|about/contact#tails-dev]]!\n"
msgstr "Precisamos da sua ajuda e existem muitas formas de [[contribuir com o Tails|contribute]] (<a href=\"https://tails.boum.org/donate/?r=3.6.2\">doar</a> é apenas uma delas). Venha [[conversar com a gente|about/contact#tails-dev]]!\n"
