# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2018-12-29 23:52+0000\n"
"PO-Revision-Date: 2018-10-26 13:14+0000\n"
"Last-Translator: Weblate Admin <admin@example.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: sr_Latn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 2.19.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Documentation\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"tip\">\n"
msgstr "<div class=\"tip\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>If this section doesn't answer your questions, you can also look at our\n"
"[[FAQ|support/faq]].</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr ""

#. type: Plain text
msgid ""
"Read about how you can help [[improving Tails documentation|/contribute/how/"
"documentation]]."
msgstr ""

#. type: Plain text
msgid "- [[Introduction to this documentation|introduction]]"
msgstr ""

#. type: Title #
#, no-wrap
msgid "General information"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/about.index\" raw=\"yes\" sort=\"age\"]]\n"
msgstr ""

#. type: Title #
#, no-wrap
msgid "Download and install"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Install from another Tails (for PC)|install/win/clone-overview]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Install from another Tails (for Mac)|install/mac/clone-overview]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Install from Windows|install/win/usb-overview]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Install from macOS|install/mac/usb-overview]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Install from Linux (recommended)|install/linux/usb-overview]]"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Install from Debian, Ubuntu, or Mint using the command line and GnuPG "
"(experts)|install/expert/usb-overview]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Burn a DVD|install/dvd]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Download without installing|install/download]]"
msgstr ""

#. type: Title #
#, no-wrap
msgid "First steps with Tails"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/first_steps.index\" raw=\"yes\" sort=\"age\"]]\n"
msgstr ""

#. type: Title #
#, no-wrap
msgid "Connect to the Internet anonymously"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/anonymous_internet.index\" raw=\"yes\" sort=\"age\"]]\n"
msgstr ""

#. type: Title #
#, no-wrap
msgid "Encryption and privacy"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/encryption_and_privacy.index\" raw=\"yes\" sort=\"age\"]]\n"
msgstr ""

#. type: Title #
#, no-wrap
msgid "Work on sensitive documents"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/sensitive_documents.index\" raw=\"yes\" sort=\"age\"]]\n"
msgstr ""

#. type: Title #
#, no-wrap
msgid "Advanced topics"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/advanced_topics.index\" raw=\"yes\" sort=\"age\"]]\n"
msgstr ""
