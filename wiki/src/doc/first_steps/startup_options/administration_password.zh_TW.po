# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2018-09-15 11:19+0000\n"
"PO-Revision-Date: 2018-11-02 08:12+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.19.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Administration password\"]]\n"
msgstr "[[!meta title=\"Administrationspasswort\"]]\n"

#. type: Plain text
msgid ""
"In Tails, an administration password (also called *root password* or "
"*amnesia password*) is required to perform system administration tasks.  For "
"example:"
msgstr ""

#. type: Bullet: '  - '
msgid "To [[install additional software|doc/first_steps/additional_software]]"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"To [[access the internal hard disks of the computer|doc/"
"encryption_and_privacy/your_data_wont_be_saved_unless_explicitly_asked]]"
msgstr ""
"為了[[接取電腦的內鍵硬碟|doc/encryption_and_privacy/"
"your_data_wont_be_saved_unless_explicitly_asked]]"

#. type: Bullet: '  - '
msgid "To execute commands with <span class=\"command\">sudo</span>"
msgstr "利用<span class=\"command\">sudo</span> 來執行命令"

#. type: Plain text
#, no-wrap
msgid ""
"**By default, the administration password is disabled for better security.**\n"
"This can prevent an attacker with physical or remote access to your Tails system\n"
"to gain administration privileges and perform administration tasks\n"
"against your will.\n"
msgstr ""
"**在默設情況下為了更佳的安全考量，管理者密碼設為關閉**\n"
"這可以防止攻擊者在違背你意志的情況下\n"
"實際接近或遠端接取你的  Tails 系統\n"
"並取得管理員特權來執行管理員任務\n"

#. type: Plain text
#, no-wrap
msgid "[[!img password-prompt.png link=\"no\" alt=\"Authentication required: amnesia password (also called *administration password* or *root password*)\"]] <!-- Note for translators: the alt tag is useful for SEO. -->\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Set up an administration password\n"
msgstr "設定一個管理員密碼\n"

#. type: Plain text
#, no-wrap
msgid ""
"In order to perform administration tasks, you need to set up an administration\n"
"password when starting Tails, using [[<span class=\"application\">Tails\n"
"Greeter</span>|startup_options#tails_greeter]].\n"
msgstr ""
"為了執行管理者任務，需要在一開始進入 Tails 時\n"
"設定一個管理者密碼[[<span class=\"application\">Tails\n"
"Greeters</span>|startup_options#tails_greeter]]。\n"

#. type: Bullet: '1. '
msgid ""
"When <span class=\"application\">Tails Greeter</span> appears, click on the "
"<span class=\"button\">[[!img lib/list-add.png alt=\"Expand\" class="
"\"symbolic\" link=\"no\"]]</span> button."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img additional.png link=\"no\" alt=\"Additional settings of Tails Greeter\"]]\n"
msgstr ""

#. type: Bullet: '2. '
msgid ""
"When the <span class=\"guilabel\">Additional Settings</span> dialog appears, "
"click on <span class=\"guilabel\">Administration Password</span>."
msgstr ""

#. type: Bullet: '3. '
msgid ""
"Specify a password of your choice in both the <span class=\"guilabel"
"\">Administration Password</span> and <span class=\"guilabel\">Confirm</"
"span> text boxes then click <span class=\"button\">Add</span>."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"open_root_terminal\"></a>\n"
msgstr "<a id=\"open_root_terminal\"></a>\n"

#. type: Title =
#, no-wrap
msgid "How to open a root terminal\n"
msgstr "如何開啟超級管理員終端\n"

#. type: Plain text
msgid ""
"To open a root terminal during your working session, you can do any of the "
"following:"
msgstr "在工作期間要開啟超級管理員終端器，可以有下列方法："

#. type: Plain text
#, no-wrap
msgid ""
"  - Choose\n"
"    <span class=\"menuchoice\">\n"
"      <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"      <span class=\"guisubmenu\">System Tools</span>&nbsp;▸\n"
"      <span class=\"guimenuitem\">Root Terminal</span></span>.\n"
msgstr ""
"  - 選擇\n"
"    <span class=\"menuchoice\">\n"
"      <span class=\"guimenu\">應用程式</span>&nbsp;▸\n"
"      <span class=\"guisubmenu\">系統工具</span>&nbsp;▸\n"
"      <span class=\"guimenuitem\">超級管理員終端</span></span>.\n"

#. type: Bullet: '  - '
msgid "Execute <span class=\"command\">sudo -i</span> in a terminal."
msgstr "在終端機底下，執行<span class=\"command\">sudo -i</span>指令。"
