# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2019-05-14 16:29+0000\n"
"PO-Revision-Date: 2018-07-02 06:22+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: zh\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.10.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Encrypted persistence\"]]\n"
msgstr ""

#. type: Plain text
msgid ""
"If you start Tails from a USB stick, you can create a persistent volume in "
"the free space left on the USB stick.  The files in the persistent volume "
"are saved encrypted and remain available across separate working sessions."
msgstr ""

#. type: Plain text
msgid "You can use this persistent volume to store any of the following:"
msgstr ""

#. type: Bullet: '  - '
msgid "Personal files"
msgstr ""

#. type: Bullet: '  - '
msgid "Some settings"
msgstr ""

#. type: Bullet: '  - '
msgid "Additional software"
msgstr ""

#. type: Bullet: '  - '
msgid "Encryption keys"
msgstr ""

#. type: Plain text
msgid ""
"The persistent volume is an encrypted partition protected by a passphrase."
msgstr ""

#. type: Plain text
msgid ""
"Once the persistent volume is created, you can choose to activate it or not "
"each time you start Tails."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/first_steps/persistence.caution\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/first_steps/persistence.caution.zh\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"How to use the persistent volume\n"
"=================================\n"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Warnings about persistence|first_steps/persistence/warnings]]"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Create & configure the persistent volume|first_steps/persistence/"
"configure]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Enable & use the persistent volume|first_steps/persistence/use]]"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Change the passphrase of the persistent volume|first_steps/persistence/"
"change_passphrase]]"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Manually copy your persistent data to a new USB stick|first_steps/"
"persistence/copy]]"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Check the file system of the persistent volume|first_steps/persistence/"
"check_file_system]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Delete the persistent volume|first_steps/persistence/delete]]"
msgstr ""

#~ msgid "<div class=\"note\">\n"
#~ msgstr "<div class=\"note\">\n"

#~ msgid "</div>\n"
#~ msgstr "</div>\n"
