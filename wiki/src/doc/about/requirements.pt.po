# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2019-07-31 11:16+0000\n"
"PO-Revision-Date: 2018-04-09 04:48-0300\n"
"Last-Translator: Tails translators <tails@boum.org>\n"
"Language-Team: \n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"System requirements\"]]\n"
msgstr "[[!meta title=\"Requisitos de sistema\"]]\n"

#. type: Plain text
msgid "Tails works on most computers less than 10 years old."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"note\">\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<p>Tails might not work on:</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<ul>\n"
"<li>Some older computers, for example if they don't have enough\n"
"RAM.</li>\n"
"<li>Some newer computers, for example if their [[graphics card is\n"
"not compatible with Linux|support/known_issues/graphics]].</li>\n"
"</ul>\n"
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "<div class=\"note\">\n"
#| "See the [[known hardware compatibility issues|support/known_issues]].\n"
#| "</div>\n"
msgid ""
"<p>See the [[known hardware compatibility\n"
"issues|support/known_issues]].</p>\n"
msgstr ""
"<div class=\"note\">\n"
"Veja os [[problemas de compatibilidade de hardware conhecidos|support/known_issues]].\n"
"</div>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr ""

#. type: Plain text
msgid "Hardware requirements:"
msgstr ""

#. type: Plain text
msgid "- A USB stick of 8 GB minimum or a DVD recordable."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  All the data on this USB stick or DVD will be lost when installing Tails.\n"
msgstr ""

#. type: Plain text
msgid "- The possibility to start from a USB stick or a DVD reader."
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "Tails requires a 64-bit <span class=\"definition\">[[!wikipedia x86-64]]</span> compatible processor: **<span class=\"definition\">[[!wikipedia IBM_PC_compatible]]</span>** and others but not <span class=\"definition\">[[!wikipedia PowerPC]]</span> nor <span class=\"definition\">[[!wikipedia ARM_architecture desc=\"ARM\"]]</span>. Mac computers are IBM PC compatible since 2006. Tails does **not** run on most tablets and phones."
msgid ""
"- A 64-bit <span class=\"definition\">[[!wikipedia x86-64]]</span>\n"
"  compatible processor:\n"
"  - <span class=\"definition\">[[!wikipedia IBM_PC_compatible]]</span> but not\n"
"    <span class=\"definition\">[[!wikipedia PowerPC]]</span> nor\n"
"    <span class=\"definition\">[[!wikipedia ARM_architecture desc=\"ARM\"]]</span>.\n"
"  - Mac computers are IBM PC compatible since 2006.\n"
"  - Tails does not run on most tablets and phones.\n"
msgstr "Tails requer um processador de 64 bits compatível com <span class=\"definition\">[[!wikipedia x86-64]]</span>: **<span class=\"definition\">[[!wikipedia_pt IBM_PC_compatível desc=\"compatíveis com IBM PC\"]]</span>** e outros, mas não <span class=\"definition\">[[!wikipedia_pt PowerPC]]</span> e nem <span class=\"definition\">[[!wikipedia_pt Arquitetura_ARM desc=\"ARM\"]]</span>. Computadores Mac são compatíveis com IBM PC desde 2006. Tails **não** funciona na maioria dos tablets e telefones."

#. type: Plain text
msgid "- 2 GB of RAM to work smoothly."
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "**2 GB of RAM** to work smoothly. Tails is known to work with less memory but you might experience strange behaviours or crashes."
msgid "  Tails can work with less than 2 GB RAM but might behave strangely or crash.\n"
msgstr "**2 GB de RAM** para funcionar suavemente. É sabido que o Tails funciona com menos memória, mas você poderá experienciar comportamentos estranhos ou travamentos."

#~ msgid ""
#~ "Tails works on most reasonably recent computers, say manufactured after "
#~ "2008.  Here is a detailed list of requirements:"
#~ msgstr ""
#~ "Tails funciona em qualquer computador razoavelmente recente, digamos, "
#~ "fabricado após 2008. Aqui está a lista detalhada de requisitos:"

#~ msgid ""
#~ "Either **an internal or external DVD reader** or the possibility to "
#~ "**boot from a USB stick**."
#~ msgstr ""
#~ "Um **leitor de DVD interno ou externo** ou a possibilidade de **iniciar a "
#~ "partir de uma memória USB**."
